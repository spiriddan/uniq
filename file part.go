package main

import (
	"bufio"
	"io"
	"os"
	"strconv"
	"strings"
)

func importDictFromFile(dict map[string]int, fi int, fOffset, sOffset int, filename string) ([]string, error) {
	_, e := os.Stat(filename)
	e = ErrorProcessing(e)
	if e != nil {
		return nil, e
	}

	fin, _ := os.Open(filename)

	reader := bufio.NewScanner(fin)
	reader.Split(bufio.ScanLines)
	res := []string{}

	line := reader.Text()

	for reader.Scan() {

		if fi != 0 { // не учитывать регистр
			line = strings.ToLower(line)
		}

		// отступы
		line, _ = setLineOffsets(line, fOffset, sOffset)

		// добавить в словарь
		_, mNameExist := dict[line]
		if mNameExist {
			dict[line]++
		} else {
			res = append(res, line)
			dict[line] = 1
		}

		line = reader.Text()
	}

	defer fin.Close()

	return res, nil
}

func outputToFile(lines map[string]int, uniqLines []string, ne1, ne2, ne3 bool, filename string) (e error) {
	fout, e := os.OpenFile(filename, os.O_WRONLY, 0777)
	if e != nil {
		println(e.Error(), "\nCreating file...")
		_, e = os.Create(filename)
		if e != nil {
			println(e.Error())
		}
		println("Created file")
	}

	writer := bufio.NewWriter(fout)
	k, _ := writer.WriteString("fpsaodfiaspoifaspdoifdospifpoasdifposadifposidfopisdf")
	println(k)
	if ne1 { //c
		for i := range uniqLines {
			_, e = io.WriteString(writer, strconv.Itoa(lines[uniqLines[i]])+"."+uniqLines[i])
			//_, e = writer.WriteString(strconv.Itoa(lines[uniqLines[i]]) + "." + uniqLines[i])

		}
	} else if ne2 { //d
		for i := range uniqLines {
			if lines[uniqLines[i]] > 1 {
				_, e = writer.WriteString(uniqLines[i])
			}
		}
	} else if ne3 { // u
		for i := range uniqLines {
			if lines[uniqLines[i]] == 1 {
				_, e = writer.WriteString(uniqLines[i])
			}
		}
	} else { // просто вывод каждой уникальной строчки
		for i := range uniqLines {
			_, e = io.WriteString(writer, uniqLines[i])
			//_, e = writer.WriteString(uniqLines[i])
		}
	}
	e = ErrorProcessing(e)

	defer fout.Close()
	println("Written")
	return e
}
