package main

import (
	"errors"
	"os"
	"strconv"
	"strings"
)

const (
	hugeBoy = 1000
)

func Find(a [6]string, x string) (int, error) {
	for i, n := range a {
		if x == n {
			return i, nil
		}
	}
	return len(a), nil
}

func setLineOffsets(line string, fOffset, sOffset int) (string, error) {
	if fOffset != 0 {
		splited := strings.Split(line, " ")
		line = ""
		for i := fOffset; i < len(splited); i++ {
			line += splited[i]
		}
	}

	if sOffset != 0 {
		line = line[sOffset:]
	}
	return line, nil
}

func analyzeFlags() (fOffset, sOffset int, inputWay string, outputWay string, flagsOnReady map[string]int, e error) {
	args := os.Args[1:]
	flags := [...]string{"-c", "-d", "-u", "-i", "-f", "-s"}
	flagsOnReady = make(map[string]int, 6)

	// get flags
	for i := 0; i < len(args); i++ {
		BOOBA, _ := Find(flags, args[i])
		if len(args[i]) > 6 { // input / output files
			if args[i][len(args[i])-4:] == ".txt" { // 6: перенос строки, возврат каретки + .txt
				inputWay = args[i]

				if (len(args) != i+1) && (len(args[i+1]) > 4) && (args[i+1][len(args[i+1])-4:] == ".txt") {
					outputWay = args[i+1]
					i++
				}
				i++

			}
		} else if BOOBA == 6 {
			println("DAMN DUDE WHAT THE FUCK IS", args[i])
			e = errors.New("wrong key")
			return
		} else if args[i] == "-f" {
			fOffset, _ = strconv.Atoi(args[i+1])
			i++
		} else if args[i] == "-s" {
			sOffset, _ = strconv.Atoi(args[i+1])
			i++
		} else {
			flagsOnReady[args[i]] = 1
		}
	}

	if flagsOnReady["-c"]+flagsOnReady["-d"]+flagsOnReady["-u"] > 3 {
		e = errors.New("you can use only one -c/-d/-u key at time")
	}
	return
}

func output(flagsOnReady map[string]int, lines map[string]int, uniqLines []string, outputWay string) (e error) {
	_, mNameExist1 := flagsOnReady["-c"]
	_, mNameExist2 := flagsOnReady["-d"]
	_, mNameExist3 := flagsOnReady["-u"]
	if outputWay == "" {
		e = outputToConsole(lines, uniqLines, mNameExist1, mNameExist2, mNameExist3)
	} else {
		e = outputToFile(lines, uniqLines, mNameExist1, mNameExist2, mNameExist3, outputWay)
	}

	e = ErrorProcessing(e)

	return nil
}

func input(flagsOnReady map[string]int, fOffset, sOffset int, inputWay string) ([]string, map[string]int, error) {
	var uniqLines []string
	var e error
	lines := make(map[string]int, hugeBoy)

	if inputWay == "" {
		uniqLines, e = inputDictFromConsole(lines, flagsOnReady["-i"], fOffset, sOffset)
	} else {
		uniqLines, e = importDictFromFile(lines, flagsOnReady["-i"], fOffset, sOffset, inputWay)
	}

	e = ErrorProcessing(e)
	return uniqLines, lines, e
}

func ErrorProcessing(e error) error {
	if e != nil {
		println("AHTUNG! AHTUNG!")
		println(e.Error())
	}
	return e
}

func main() { // ek makarek git day mne yzhe mr sdelat'
	fOffset, sOffset, inputWay, outputWay, flagsOnReady, e := analyzeFlags()
	if ErrorProcessing(e) != nil {
		return
	}

	uniqLines, lines, _ := input(flagsOnReady, fOffset, sOffset, inputWay)

	_ = output(flagsOnReady, lines, uniqLines, outputWay)
}
