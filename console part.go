package main

import (
	"bufio"
	"os"
	"strings"
)

func inputDictFromConsole(dict map[string]int, fi int, fOffset, sOffset int) ([]string, error) {
	reader := bufio.NewReader(os.Stdin)
	res := []string{}

	println("Input text. When you finish type 'exit' (в другую строчку напечатай)\n")
	line, _ := reader.ReadString('\n')

	for strings.Compare(line[:len(line)-2], "exit") != 0 {

		if fi != 0 { // не учитывать регистр
			line = strings.ToLower(line)
		}

		// отступы
		line, _ = setLineOffsets(line, fOffset, sOffset)

		// добавить в словарь
		_, mNameExist := dict[line]
		if mNameExist {
			dict[line]++
		} else {
			res = append(res, line)
			dict[line] = 1
		}

		line, _ = reader.ReadString('\n')
	}

	return res, nil
}

func outputToConsole(lines map[string]int, uniqLines []string, ne1, ne2, ne3 bool) error {
	println("U'r lines:")
	if ne1 { //c
		for i := range uniqLines {
			println(lines[uniqLines[i]], ".", uniqLines[i])
		}
	} else if ne2 { //d
		for i := range uniqLines {
			if lines[uniqLines[i]] > 1 {
				println(uniqLines[i])
			}
		}
	} else if ne3 { // u
		for i := range uniqLines {
			if lines[uniqLines[i]] == 1 {
				println(uniqLines[i])
			}
		}
	} else { // просто вывод каждой уникальной строчки
		for i := range uniqLines {
			println(uniqLines[i])
		}
	}

	return nil
}
